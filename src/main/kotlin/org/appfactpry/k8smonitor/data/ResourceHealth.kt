package org.appfactpry.k8smonitor.data

import org.slf4j.Logger
import org.slf4j.LoggerFactory

abstract class ResourceHealth<T>(val resourceName: String) {
    abstract fun merge(resourceHealth: T)
}

class PodHealth(resourceName: String, val status: String) : ResourceHealth<PodHealth>(resourceName) {
    var redinessProbeStatus: HealthEndpointStatus? = null
    var livenessProbeStatus: HealthEndpointStatus? = null

    val logger: Logger = LoggerFactory.getLogger(PodHealth::class.java)
    override fun merge(resourceHealth: PodHealth) {

    }
}

class NodeGroupHealth(
    resourceName: String,
    vararg val containerNames: String,
    val nodeGroupType: NodeGroupType,
    val desiredPodCount: Int,
    val currentPodCount: Int,
    val readyCount: Int
) : ResourceHealth<PodHealth>(resourceName) {
    var relatedPods: List<PodHealth>? = null

    override fun merge(resourceHealth: PodHealth) {

    }
}

enum class NodeGroupType {
    DAEMON_SET, REPLICA_SET
}

data class HealthEndpointStatus(val endpoint: String, val status: String, val responseTime: Long)