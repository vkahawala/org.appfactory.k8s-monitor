package org.appfactpry.k8smonitor

import org.appfactpry.k8smonitor.data.ResourceHealth
import java.util.*

interface Observer {

    fun register(observableId: String)

    fun update(observableId: String, transactionId: String, updateCompleted: Boolean, updatedValue: ResourceHealth<*>)
    fun updateError(observableId: String, transactionId: String)
}

interface Observable<T> {
    fun updateObservers(updateCompleted: Boolean, value: T)

    fun getObservableId(): String = UUID.randomUUID().toString()

    fun registerObserver(observer: Observer) {
        observer.register(this.getObservableId())
    }
}