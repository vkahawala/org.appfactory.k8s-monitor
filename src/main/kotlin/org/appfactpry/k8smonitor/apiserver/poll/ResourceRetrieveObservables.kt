package org.appfactpry.k8smonitor.apiserver.poll

import org.appfactpry.k8smonitor.Observable
import org.appfactpry.k8smonitor.Observer
import org.appfactpry.k8smonitor.data.PodHealth
import java.util.concurrent.Callable

class PodListRetrievalManager(private val transactionId: String, private vararg val observers: Observer) : Runnable,
    Observable<PodHealth> {
    init {
        observers.iterator().forEach { registerObserver(it) }
    }

    override fun run() {
    }


    override fun updateObservers(updateCompleted: Boolean, value: PodHealth) {
        observers.iterator().forEach { it.update(getObservableId(), transactionId, updateCompleted, value) }
    }
}

class PodRetrievalManager(
    private val transactionId: String,
    private vararg val observers: Observer,
    private val podName: String,
    private val namespace: String
) : Callable<PodHealth>,
    Observable<PodHealth> {
    init {
        observers.iterator().forEach { registerObserver(it) }
    }

    override fun call(): PodHealth {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun updateObservers(updateCompleted: Boolean, value: PodHealth) {
        observers.iterator().forEach { it.update(getObservableId(), transactionId, updateCompleted, value) }
    }
}