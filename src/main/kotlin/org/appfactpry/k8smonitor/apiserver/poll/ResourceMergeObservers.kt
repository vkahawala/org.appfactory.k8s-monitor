package org.appfactpry.k8smonitor.apiserver.poll

import org.appfactpry.k8smonitor.Observer
import org.appfactpry.k8smonitor.data.NodeGroupHealth
import org.appfactpry.k8smonitor.data.NodeGroupType
import org.appfactpry.k8smonitor.data.PodHealth
import org.appfactpry.k8smonitor.data.ResourceHealth
import org.appfactpry.k8smonitor.persistence.PersistenceService
import org.appfactpry.k8smonitor.persistence.dto.MergeResult
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.util.concurrent.ConcurrentHashMap


abstract class HealthResultsMerger(private val persistenceService: PersistenceService) : Observer {
    private var observables: MutableList<String> = ArrayList()
    private var observableValues: MutableMap<String, ObservableResultSet> = ConcurrentHashMap()
    var groupObservable: String? = null

    override fun register(observableId: String) {
        observables.add(observableId)
    }

    override fun update(
        observableId: String, transactionId: String, updateCompleted: Boolean, updatedValue: ResourceHealth<*>
    ) {
        val observableResultSet: ObservableResultSet = getObservableResultSet(transactionId)

        with(observableResultSet) { updatedValues[observableId] ?: ArrayList() }
        val updatedValuesPerObservable = observableResultSet.updatedValues[observableId]
        updatedValuesPerObservable?.add(updatedValue)
        if (updateCompleted) {
            observableResultSet.markObservableCompleted(observableId)
        }

        if (observableResultSet.observableCompleted.size == getObservableCount()) {
            observableValues.remove(transactionId)
            if (!observableResultSet.error) {
                persistenceService.persist(merge(observableResultSet))
            }
        }
    }

    override fun updateError(observableId: String, transactionId: String) {
        val observableResultSet: ObservableResultSet = getObservableResultSet(transactionId)
        observableResultSet.error = true
    }

    protected abstract fun merge(resultSet: ObservableResultSet): MergeResult
    protected abstract fun identifyMergeGroup(observableId: String, updatedValue: ResourceHealth<*>)
    private fun getObservableCount(): Int = observables.size

    private fun getObservableResultSet(transactionId: String): ObservableResultSet {
        return observableValues[transactionId]
            ?: ObservableResultSet(transactionId).also { observableValues[transactionId] = it }
    }
}

@Component
class PodHealthObserver(@Autowired val persistenceService: PersistenceService) :
    HealthResultsMerger(persistenceService) {
    override fun merge(resultSet: ObservableResultSet): MergeResult {
        return MergeResult(resultSet.updatedValues.values.elementAt(0)[0])
    }

    override fun identifyMergeGroup(observableId: String, updatedValue: ResourceHealth<*>) {
        if (updatedValue is PodHealth)
            groupObservable = observableId
    }
}

@Component
class DaemonSetPodMerger(@Autowired val persistenceService: PersistenceService) :
    HealthResultsMerger(persistenceService) {
    override fun merge(resultSet: ObservableResultSet): MergeResult {
        with(resultSet) {
            val nodeGroupHealth = updatedValues[groupObservable]?.elementAt(0)
            for ((k, v) in updatedValues) {
                for (result in v)
                    nodeGroupHealth?.merge(result)
            }
        }
    }

    override fun identifyMergeGroup(observableId: String, updatedValue: ResourceHealth<*>) {
        if (updatedValue is NodeGroupHealth && updatedValue.nodeGroupType == NodeGroupType.DAEMON_SET)
            groupObservable = observableId
    }
}

class ObservableResultSet(val transactionId: String) {
    var updatedValues: MutableMap<String, MutableList<ResourceHealth<*>>> = HashMap()
    var observableCompleted: MutableMap<String, Boolean> = HashMap()
    var mergeResult: MergeResult? = null
    var error: Boolean = false

    fun markObservableCompleted(observableId: String) {
        observableCompleted[observableId] = true
    }
}