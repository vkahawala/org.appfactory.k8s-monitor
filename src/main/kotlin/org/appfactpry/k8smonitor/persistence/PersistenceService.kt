package org.appfactpry.k8smonitor.persistence

import org.appfactpry.k8smonitor.persistence.dto.MergeResult

interface PersistenceService {
    fun persist(mergeResult: MergeResult)
}