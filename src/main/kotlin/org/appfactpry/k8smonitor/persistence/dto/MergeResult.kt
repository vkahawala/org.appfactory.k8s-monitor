package org.appfactpry.k8smonitor.persistence.dto

import org.appfactpry.k8smonitor.data.ResourceHealth

class MergeResult(val resourceHealth: ResourceHealth<*>)