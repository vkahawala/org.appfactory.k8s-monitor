package org.appfactpry.k8smonitor.persistence

import org.appfactpry.k8smonitor.persistence.service.InfluxPersistenceService
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class Configuration(@Value("\$persistence.type") val persistenceType: String) {

    @Bean
    fun persistenceService(): PersistenceService {
        if (persistenceType == "influx") {
            return InfluxPersistenceService()
        }
        throw RuntimeException("PersistenceService not implemented for the type $persistenceType")
    }
}